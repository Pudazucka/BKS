#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

enum {SECS_TO_SLEEP = 2, NSEC_TO_SLEEP = 0};

void my_signal_handler(int signal_number)
{
	(void)signal_number;
}


int main(void) {
	struct timespec remaining, request = {SECS_TO_SLEEP, NSEC_TO_SLEEP};

	struct sigaction sa_register = {
		.sa_handler = &my_signal_handler,
		.sa_flags = 0,
	};
	sigemptyset(&sa_register.sa_mask);
	printf("getpid = %d\n", getpid());
	int err = sigaction(SIGINT, &sa_register, NULL);
	// error handling recommended but ommitted here.
	while (1) {
			printf("Lulz, u suck! try again\n");
			nanosleep(&request, &remaining);
	}

	exit(err);
}

