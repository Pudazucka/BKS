#include <stdio.h>
#include <unistd.h>

int main (int argc, char **argv)
{
	FILE *fp;
	char c = '0';
	fp = fopen(argv[1], "r");
	if (argc == 1) 
	{
		printf("No file name given!");
		return 1;
	}
	if (!fp){
		printf("No file named '%s' found!", argv[1]);
		return 1;
	}
	while(c != EOF)
	{	
		c = fgetc(fp);
		printf("%c", c);
	}
	fclose(fp);
	return 0;
}
